package Serialization;

import IOService.IOStreamService;
import org.junit.Test;

import java.io.*;

import static Serialization.HouseService.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class HouseServiceTest {

    @Test
    public void testReadWriteByte() {
        int[] array = new int[]{2, 1, 1, 1};
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            IOStreamService.writeOutputStream(bos, array);
            assertArrayEquals(array, IOStreamService.readInputStream(new ByteArrayInputStream(bos.toByteArray()), array));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testReadWrite() {
        int[] array = new int[]{2, 1, 2, 3};
        try (StringWriter sw = new StringWriter()) {
            IOStreamService.writeCharArrayWriter(sw, array);
    //        assertArrayEquals(array, IOStreamService.readerCharArrayReader(new BufferedReader(), array));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSerialiseHouse() {
        House house = new House("12345", "г.Омск, ул.Ленина, д.123",
                new Person("Иванов", "Иван", "Иванович", "03.05.1950"),
                new Flat(10, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(27, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81"),
                        new Person("Неиванов", "Неиван", "Неиванович", "10.05.83")),
                new Flat(14, 53, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(20, 48, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")));
        serialiseHouse(house, "serializedHouse.txt");
        assertEquals(house, deserializeHouse("serializedHouse.txt"));
    }

    @Test
    public void testJacksonSer() throws IOException {
        House house = new House("12345", "г.Омск, ул.Ленина, д.123",
                new Person("Иванов", "Иван", "Иванович", "03.05.1979"),
                new Flat(10, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(27, 45, new Person("Романов", "Анатолий", "Владимирович", "30.09.81"),
                        new Person("Неиванов", "Неиван", "Неиванович", "10.05.83")),
                new Flat(14, 53, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")),
                new Flat(20, 48, new Person("Романов", "Анатолий", "Владимирович", "30.09.81")));


        String ser = serializeHouseToJacksonString(house);
        House deser = deserializeHouseFromJacksonString(ser);
        assertEquals(house, deser);
    }
}
