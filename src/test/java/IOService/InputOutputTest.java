package IOService;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.assertArrayEquals;

public class InputOutputTest {
    @Test
    public void testReadWriteByte() throws IOException {
        int[] array = new int[]{2, 1, 1, 1};
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            IOStreamService.writeOutputStream(bos, array);
            assertArrayEquals(array, IOStreamService.readInputStream(new ByteArrayInputStream(bos.toByteArray()), array));
        }
    }

    @Test
    public void testReadWrite() {
        int[] array = new int[]{257, 81, 12, 13};
        try (StringWriter sw = new StringWriter()) {
            IOStreamService.writeCharArrayWriter(sw, array);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
