package IOService;

import java.io.*;
import java.util.ArrayList;

public class IOStreamService {

    /*Записать массив целых чисел в двоичный поток. Предполагается, что до чтения массив уже создан, 
    нужно прочитать n чисел, где n — длина массива. */
    
    public static void writeOutputStream(OutputStream out, int [] array)throws IOException {
        try( DataOutputStream dos = new DataOutputStream(out)) {
            for (int elem : array) {
                dos.writeInt(elem);
            }
        }
    }
    
    /*Прочитать массив целых чисел из двоичного потока. Предполагается, что до чтения массив уже создан,
    нужно прочитать n чисел, где n — длина массива. */
    
    public static int [] readInputStream(InputStream in, int [] array)throws IOException{
        DataInputStream dis = new DataInputStream(in);
        for (int i = 0; i < array.length; i++){
            array[i] = dis.readInt();
        }
        return array;
    }
    
    /* Аналогично, используя символьные потоки. В потоке числа должны разделяться
    пробелами.*/
    
    public static void writeCharArrayWriter(Writer writer, int [] array)throws IOException{
        BufferedWriter printer = new BufferedWriter(writer);
        for (int elem: array){
            printer.write(String.valueOf(elem));
            printer.write(" ");
        }
    }
    
    // Чтение
    public static int[] readerCharArrayReader(Reader reader, int []array)throws IOException{
        BufferedReader bReader = new BufferedReader(reader);
        String[] strArray = bReader.readLine().split(" ");
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(strArray[i]);
        }
        return array;
    }
    
    
    
    public static void writeArrayToFile(int[] array, String path) {
        try (RandomAccessFile raf = new RandomAccessFile(path, "rw")) {
            for (int i : array) {
                raf.writeInt(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Используя класс RandomAccessFile, прочитайте массив целых чисел, начиная с заданной позиции.
    
    public static int [] readFromPosition(long position, RandomAccessFile file) throws IOException{
        int len = (int)((file.length() - position)/Integer.BYTES); 
        if (len < 1){
          throw new IOException("Not enough information for reading");
        }
        file.seek(position);
        int [] array = new int[len];
        for (int i = 0; i < len; i++){
            array[i] = file.readInt();
        }
        return array;
    }

    /*Используя класс File, получите список всех файлов с заданным расширением в заданном
    каталоге (поиск в подкаталогах выполнять не надо).*/
    
    public static File [] findFiles(String format, File directory){
        return directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File directory, String name) {
                return name.toLowerCase().endsWith(format);
            }
        });
    }

    /*Используя класс File, получите в заданном каталоге список всех файлов и подкаталогов,
    имена которых удовлетворяют заданному регулярному выражению. Поиск должен распространиться в подкаталоги.
    Имена найденных файлов должны быть вместе с абсолютными путями.*/
    
    public static File [] findFilesEx(String format, File directory){
        ArrayList<File> res = new ArrayList<>();
        File[] files = directory.listFiles();
        for(File file:files) {
            if (file.isFile() && file.getName().endsWith(format)) {
                res.add(file);
            }
        }
        return res.toArray(new File[0]);
    }
}
