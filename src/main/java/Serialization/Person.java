package Serialization;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Person implements Serializable {
    private String surname;
    private String name;
    private String patronymic;
    private String birthDate;
    public Person() {}
    public Person(String surname, String name, String patronymic, String birthDate) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
    }
    public Person(Person copy) {
        surname = copy.surname;
        name = copy.name;
        patronymic = copy.patronymic;
        birthDate = copy.birthDate;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(getSurname(), person.getSurname()) &&
                Objects.equals(getName(), person.getName()) &&
                Objects.equals(getPatronymic(), person.getPatronymic()) &&
                Objects.equals(getBirthDate(), person.getBirthDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSurname(), getName(), getPatronymic(), getBirthDate());
    }

    @Override
    public String toString() {
        return  surname + ' ' +
                  name + ' ' +
                 patronymic + ' ' +
                ", birthDate= " + birthDate;
    }
}
