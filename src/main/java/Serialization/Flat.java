package Serialization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Flat implements Serializable {
    private int flat;
    private double flatArea;
    private List<Person> tenantList;

    public Flat(){}

    public Flat(int flat, double flatArea, Person ... tenants) {
        this.flat = flat;
        this.flatArea = flatArea;
        tenantList = new ArrayList<>();
        Collections.addAll(tenantList, tenants);
    }

    public int getFlat() {
        return flat;
    }

    public void setFlat(int flat) {
        this.flat = flat;
    }

    public double getFlatArea() {
        return flatArea;
    }

    public void setFlatArea(double flatArea) {
        this.flatArea = flatArea;
    }

    public List<Person> getTenantList() {
        return tenantList;
    }

    public void setTenantList(List<Person> tenantList) {
        this.tenantList = tenantList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;
        Flat flat1 = (Flat) o;
        return getFlat() == flat1.getFlat() &&
                Double.compare(flat1.getFlatArea(), getFlatArea()) == 0 &&
                Objects.equals(getTenantList(), flat1.getTenantList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlat(), getFlatArea(), getTenantList());
    }

    @Override
    public String toString() {
        return "Flat №" + flat +
                ", flatArea=" + flatArea +
                ", Tenants" + tenantList;
    }
}
