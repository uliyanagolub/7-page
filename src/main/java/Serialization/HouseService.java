package Serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

/* сервисный класс с методами, которые сериализуют и десериализуют объект типа
House в заданный поток средствами Java.*/

public class HouseService {
    public static void serialiseHouse(House house, String fileName) {
        try (ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
            out.writeObject(house);
            out.flush(); //очищает буфер вывода
        } catch (IOException e) {
            System.out.println("Serialization error!");
        }
    }
    public static House deserializeHouse(String fileName) {
        House house = null;
        try (ObjectInput in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fileName)))) {
            house = (House)in.readObject();
            return house;
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Deserialization error!");
        }
       return house;
    }

    /* Подключите к проекту библиотеку Jackson. Напишите методы сериализации /
    десериализации объектов типа House в строки. Используйте data binding.*/

    public static String serializeHouseToJacksonString(House house) throws IOException {
        // Файл, в который будем писать:
        //Writer writer = new FileWriter("data.txt");
        ObjectMapper mapper= new ObjectMapper();
        String jsonString = mapper.writeValueAsString(house);
        //writer.close();
        return jsonString;
    }

    public static House deserializeHouseFromJacksonString(String json) throws IOException {
        //Reader reader = new FileReader("data.txt"); // откуда читаем json
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, House.class);
    }

    /* Сравнить две json-строки на равенство. Нужно учесть, что json-объект — неупорядоченный
    набор полей, сравнение должно распространяться вглубь объектов и массивов. (Подсказка:
    средствами Jackson можно разобрать json в дерево). */

    public static boolean jsonEqualDeep(String json1, String json2) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        JsonNode jsonNode1 = mapper.readTree(json1);
        JsonNode jsonNode2 = mapper.readTree(json2);

        return jsonNode1.equals(jsonNode2);
    }




}
