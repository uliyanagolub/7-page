package Serialization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {
    private String houseNumber;
    private String address;
    private Person seniorHousekeeper;
    private List<Flat> flatList;

    public House(){    }

    public House(String houseNumber, String address, Person seniorHousekeeper,  Flat ... flats) {
        this.houseNumber = houseNumber;
        this.address = address;
        this.seniorHousekeeper = seniorHousekeeper;
        flatList = new ArrayList<>();
        Collections.addAll(flatList, flats);
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getSeniorHousekeeper() {
        return seniorHousekeeper;
    }

    public void setSeniorHousekeeper(Person seniorHousekeeper) {
        this.seniorHousekeeper = seniorHousekeeper;
    }

    public List<Flat> getFlatList() {
        return flatList;
    }

    public void setFlatList(List<Flat> flatList) {
        this.flatList = flatList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof House)) return false;
        House house = (House) o;
        return Objects.equals(getHouseNumber(), house.getHouseNumber()) &&
                Objects.equals(getAddress(), house.getAddress()) &&
                Objects.equals(getSeniorHousekeeper(), house.getSeniorHousekeeper()) &&
                Objects.equals(getFlatList(), house.getFlatList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHouseNumber(), getAddress(), getSeniorHousekeeper(), getFlatList());
    }

    @Override
    public String toString() {
        return "House №'" + houseNumber + '\'' +
                ", " + address + '\'' +
                ", SeniorHousekeeper " + seniorHousekeeper +
                ", Flats: " + flatList;
    }
}
